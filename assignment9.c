#include<stdio.h>
void swap_callbyref(int*,int*);
int main()
{
int a,b;
printf("enter number a:");
scanf("%d",&a);
printf("\nenter number b:");
scanf("%d",&b);
printf("main,a=%d and b=%d",a,b);
swap_callbyref(&a,&b);
printf("\nvalues after swapping are a=%d,b=%d",a,b);
return 0;
}
void swap_callbyref(int *a,int *b)
{
int temp;
temp=*a;
*a=*b;
*b=temp;
printf("\nin function call by reference, a=%d,b=%d",*a,*b);
}